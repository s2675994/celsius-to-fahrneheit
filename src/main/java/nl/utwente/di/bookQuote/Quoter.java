package nl.utwente.di.bookQuote;

public class Quoter {
    double getCelsiusToFahrenheit(String temperature) {
        return Double.parseDouble(temperature) * 9/5 + 32;
    }
}
