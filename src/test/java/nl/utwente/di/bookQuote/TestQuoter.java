package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestQuoter {

    @Test
    public void test1DegreeCelsius() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getCelsiusToFahrenheit("1");
        Assertions.assertEquals(33.8, price, 0.0, "1degree Celsius to Fahrenheit");
    }


}
